function test() {
    if (!this.id)
        this.id = 0;

    return this.id++;
}

module.exports = {
    static: {
        info: {
            name: "Ivan",
            lastName: "Chikishev"
        },

        session: false,

        date: Date.now
    },

    dynamic: [test, function time() {
        return Date.now().toString()
    }]
};