const handlebars = require('handlebars');
const fs = require('fs');

const debug = true;

$cache = {
    get: (hbs) => {
        if (!this.cache) {
            this.cache = {};
        }

        if (debug || !this.cache[hbs]) {
            try {
                this.cache[hbs] = handlebars.compile(fs.readFileSync(`${process.cwd()}/views/${hbs}.hbs`).toString());
            } catch (e) {
                return null;
            }
        }


        return this.cache[hbs];
    },

    model: async(name) => {
        return new Promise(async(resolve) => {
            try {
                if (!this.models) {
                    this.models = {};
                }

                if (!this.models[name]) {
                    this.models[name] = require(`${process.cwd()}/models/${name}.model.js`);
                }

                let model = this.models[name], dynamic = {};


                for ($m in model.dynamic) {
                    let value = model.dynamic[$m];
                    dynamic[value.name] = await value();
                }

                resolve({static: model.static, dynamic: dynamic});
            }

            catch (e) {
                resolve({});
            }
        });
    },

    api: async(url, ctx) => {
        return new Promise(async(resolve) => {
            try {
                if (!this.api) {
                    this.api = {};
                }

                if (!this.api[url]) {
                    this.api[url] = require(`${process.cwd()}/api/${url}.js`)
                }


                let method = this.api[url][ctx.method.toLowerCase()];


                return resolve(await method(ctx));


            } catch (e) {
                return resolve(null);
            }


            return resolve(null);
        });
    }
};

function System404(ctx) {
    let view = $cache.get('404');

    ctx.status = 404;
    ctx.body = view({
        static: {
            request: ctx
        }
    });
}


module.exports = async(ctx, next) => {
    if (ctx.url == "/" || ctx.url == "/index") {
        ctx.body = $cache.get('index')(await $cache.model('default'));
    }

    else {
        try {
            const url = /\/([^\/\s]+)([^?\s]*)([?].*)?/.exec(ctx.url);

            if (url[1] == "api") {
                let api = await $cache.api(url[2], ctx);

                if (!api) {
                    ctx.status = 404;
                    return next();
                }

                ctx.body = api;
            }

            else {
                let view = $cache.get(url[0]);

                if (!view) {
                    System404(ctx);
                }

                ctx.body = view(await $cache.model(url[0]));
            }

        } catch (e) {
            System404(ctx);
        }
    }

    next();
};