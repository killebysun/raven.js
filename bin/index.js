const configure = require(`${process.cwd()}/setting.json`);


//TODO Dependencies
const koa = require('koa');
const session = require('koa-session');
const convert = require('koa-convert');
const static = require('koa-static');


//TODO Developer Dependencies


//TODO Koa Initialize Server & Plugins
const server = new koa();

server.keys = [configure.settings.keys];

server.use(convert(static(`${process.cwd()}/public`)));
server.use(convert(session(configure.settings.session, server)));
server.use(require('./router.dependencies'));



server.listen(configure.settings.http.port,
    configure.settings.http.ip, () => {
        console.log("Server is running..");
    });