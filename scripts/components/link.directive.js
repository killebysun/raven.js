module.exports = function () {
    'ngInject';

    
    return {
        restrict: 'A',
        link : (scope, element, attr) => {
            element.bind('click', () => {
                document.location = attr['rnLink'];
            });
        }
    };
};