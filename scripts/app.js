var angular = require('angular');
var router = require('angular-ui-router');
var constance = require('./consntance.view');

function configure($interpolateProvider, $stateProvider, $locationProvider) {
    'ngInject';


    //TODO Angular Global Settings
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');


    //TODO Angular UI Router Settings
    var routes = [
        {url: '/', name: 'home', templateUrl: "/pages/default"},
        {url: '/catalog/books', name: 'books', templateUrl: '/pages/books'}
    ];

    for (routeIndex in routes) {
        $stateProvider.state(routes[routeIndex]);
    }


    $locationProvider.html5Mode(true);
}


var app = angular.module('ui', [router])
    .directive('rnLink', require('./components/link.directive'))


app.config(configure);